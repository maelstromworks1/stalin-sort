use rand::{thread_rng, Rng, distributions::Uniform};

fn stalin_sort(inputarray: &Vec<i8>) -> Vec<i8> {
    
    let mut outputarray: Vec<i8> = vec![];

    let mut previous = inputarray[0].clone();
    outputarray.push(previous.clone());

    for element in &inputarray[1..] {
        if element > &previous {
            outputarray.push(element.clone());
            previous = element.clone();
        };
    };
    outputarray
}

fn main() {
    /*
     * Generates a vector of length 100 filled with random values, and performs
     * a Stalin sort where elements that do not conform to order are purged
     */
    let range = Uniform::from(-128..127);
    let mut initial_array: Vec<i8> = thread_rng()
                                        .sample_iter(&range)
                                        .take(100)
                                        .collect();
    println!("Initial array: \n{:?}", initial_array);
    let sorted_array = stalin_sort(&mut initial_array);
    println!("Purged array: \n{:?}", sorted_array);
}
